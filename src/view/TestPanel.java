package view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import controller.GameController;

public class TestPanel extends JPanel 
					implements ActionListener{
	private static final long serialVersionUID = -4236642932453746731L;
	private JRadioButton none, test1, test2;
	public TestPanel () {
		setOpaque(false);
		setLayout(new FlowLayout(FlowLayout.CENTER));
		
		none = new JRadioButton("Normal");
		none.setOpaque(false);
		none.addActionListener(this);
        none.setActionCommand("Normal");
        none.setSelected(true);
 
        test1 = new JRadioButton("Test 1");
        test1.setOpaque(false);
		test1.addActionListener(this);
        test1.setActionCommand("Test 1");
 
        test2 = new JRadioButton("Test 2");
        test2.setActionCommand("Test 2");
		test2.addActionListener(this);
        test2.setOpaque(false);
        
        
        
        add(new JLabel("Load test position:"));
        add(none);
        add(test1);
        add(test2);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if(action.equals("Normal")) {
			test1.setSelected(false);
			test2.setSelected(false);
			GameController.testGame1 = false;
			GameController.testGame2 = false;
			return;
		}
		if(action.equals("Test 1")) {
			test2.setSelected(false);
			none.setSelected(false);
			GameController.testGame1 = true;
			GameController.testGame2 = false;
			return;
		}
		if(action.equals("Test 2")) {
			none.setSelected(false);
			test1.setSelected(false);
			GameController.testGame1 = false;
			GameController.testGame2 = true;
			return;
		}
	}
}
