/*
 * Dots and Boxes
 * Submitted for the Degree of B.Sc. in Computer Science, 2010/2011
 * University of Strathclyde
 * Department of Computer and Information Sciences
 * @author Philip Rodgers
 */
package players;

import interfaces.GameState;
import data.Line;

public class HumanPlayer extends AbstractPlayer {

	private Line line;
	
	@Override
	public synchronized Line makeMove(GameState gs) {
		interrupted = false;
		line = null;

		/*
		 *  Humans make moves by clicking on the GUI.
		 *  This method waits for the user's choice to
		 *  come through from the GUI then returns it
		 *  to the GameController.
		 */
		while(line == null || interrupted) {
			try {
				wait(500);
			} catch (InterruptedException e) {}
		}
		return line;
	}
	
	@Override
	public String getDescription() {
		return "This player is used for interactive games";
	}

	@Override
	public synchronized void sendLine(Line line) {
		this.line = line;
		notify();
	}

	@Override
	public String getName() {
		return "Human";
	}
	
	@Override
	public void reset() {
		interrupt();
	}
}
