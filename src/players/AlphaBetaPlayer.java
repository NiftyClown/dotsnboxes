package players;

import interfaces.GameState;
import data.Line;

/**
 * A player which implements MiniMax with alphabeta pruning when
 * making it's moves.
 * 
 * @author Barry
 *
 */
public class AlphaBetaPlayer extends AbstractPlayer {

	/**
	 * Default constructor
	 */
	public AlphaBetaPlayer() {
		super();
	}
	
	@Override
	public Line makeMove(GameState gs) {
		/* Variables */
		int bestScore = -1;
		Line bestLine = null;
		interrupted = false;
	
		/*
		 *  Sleep for half a second.  This simply stops AI vs. AI
		 *  matches being over in a flash.
		 */
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {}
		
		if (interrupted) return null;
		
		// Go through all the successor states
		for (Line ls : gs.getRemainingLines()) {
			// Value of adding this line
			int val;
			
			// Clone of the game
			GameState clone = gs.clone();
			clone.addLine(ls);
			
			// Get the value of this addition
			val = miniMax(clone, Integer.MIN_VALUE, Integer.MAX_VALUE);

			// Check if it is the best 
			if (val > bestScore) {
				bestScore = val;
				bestLine = ls;
			}
		}
		
		return bestLine;
	}
	
	@Override
	public String getDescription() {
		return "This player will play at a level " +
				"suitable for advanced players.";
	}

	@Override
	public String getName() {
		return "AlphaBeta Player";
	}
	
	@Override
	public void reset() {
		interrupt();
	}
	
	/**
	 * @param gs
	 * 		The current game state
	 * @param alpha
	 * 		The current value of alpha.
	 * 		(The best alternative for max)
	 * @param beta
	 * 		The current value of beta.
	 * 		(The best alternative for min)
	 * @return
	 * 		The value of the state
	 */
	private int miniMax(GameState gs, int alpha, int beta) {

		// Check for terminal state
		if(gs.expand().isEmpty()) {
			return gs.getValue(); 
		}
		// Figure out who's turn it is
		if(gs.getPlayer() == 1) { 
			return getMaxValue(gs, alpha, beta); 
		}
		return getMinValue(gs, alpha, beta);
	}
	
	/**
	 * @param gs
	 * 		The current game state.
	 * @param alpha
	 * 		The current value of alpha.
	 * 		(The best alternative for max)
	 * @param beta
	 * 		The current value of beta.
	 * 		(The best alternative for min)
	 * @return
	 * 		The game state with the best heuristic value for this player.
	 */
	private int getMaxValue(GameState gs, int alpha, int beta) {
		
		// Minimal game value for max
		int val = Integer.MIN_VALUE;
		// Go through all successor states
		for (GameState ss : gs.expand()) {

			int currVal = miniMax(ss, alpha, beta);
			
			if (currVal > val) {
				val = currVal;
			}
			if (currVal >= beta) {
				return val;
			}
			if (currVal > alpha) {
				alpha = currVal;
			}

		}
		
		// Return
		return val;
	}
	
	/**
	 * @param gs
	 * 		The current game state.
	 * @param alpha
	 * 		The current value of alpha.
	 * 		(The best alternative for max)
	 * @param beta
	 * 		The current value of beta.
	 * 		(The best alternative for min)
	 * @return
	 * 		The game state with the worst heuristic value for this player.
	 * 		(Simulates the other player)
	 */
	private int getMinValue(GameState gs, int alpha, int beta) {

		// Maximal game value for min
		int val = Integer.MAX_VALUE;
		// Go through all successor states
		for (GameState ss : gs.expand()) {
			
			int currVal = miniMax(ss, alpha, beta);

			if (currVal < val) {
				val = currVal;
			}
			if (currVal <= alpha) {
				return val;
			}
			if (currVal < beta) {
				beta = currVal;
			}
		}
		
		// Return
		return val;
	}

}
