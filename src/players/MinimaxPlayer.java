package players;

import interfaces.GameState;
import data.Line;

public class MinimaxPlayer extends AbstractPlayer {

	/**
	 * Default constructor
	 */
	public MinimaxPlayer() {
		super();
	}

	@Override
	public Line makeMove(GameState gs) {
		/* Variables */
		int bestScore = -1;
		Line bestLine = null;
		interrupted = false;

		/*
		 *  Sleep for half a second.  This simply stops AI vs. AI
		 *  matches being over in a flash.
		 */
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {}

		if (interrupted) return null;

		// Go through all the successor states
		for (Line ls : gs.getRemainingLines()) {
			// Value of adding this line
			int val;

			// Clone of the game
			GameState clone = gs.clone();
			clone.addLine(ls);

			// Get the value of this addition
			val = miniMax(clone);

			// Check if it is the best 
			if (val > bestScore) {
				bestScore = val;
				bestLine = ls;
			}
		}

		return bestLine;
	}

	@Override
	public String getDescription() {
		return "This player will play at a level " +
				"suitable for moderate players.";
	}

	@Override
	public String getName() {
		return "Minimax Player";
	}

	@Override
	public void reset() {
		interrupt();
	}

	/**
	 * @param gs
	 * 		The current game state
	 * @return
	 * 		The value of the state
	 */
	private int miniMax(GameState gs) {
		// Check for terminal state
		if(gs.expand().isEmpty()) {
			return gs.getValue(); 
		}
		// Figure out who's turn it is
		if(gs.getPlayer() == 1) { 
			return getMaxValue(gs); 
		}
		return getMinValue(gs);
	}

	/**
	 * @param gs
	 * 		The current game state.
	 * @return
	 * 		The game state with the best heuristic value for this player.
	 */
	private int getMaxValue(GameState gs) {

		// Minimal game value for max
		int val = Integer.MIN_VALUE;
		// Go through all successor states
		for (GameState ss : gs.expand()) {

			int currVal = miniMax(ss);

			// Is it the best we've seen so far?
			if (currVal > val) {
				val = currVal;
			}
		}

		// Return
		return val;
	}

	/**
	 * @param gs
	 * 		The current game state.
	 * @return
	 * 		The game state with the worst heuristic value for this player.
	 * 		(Simulates the other player)
	 */
	private int getMinValue(GameState gs) {

		// Maximal game value for min
		int val = Integer.MAX_VALUE;
		// Go through all successor states
		for (GameState ss : gs.expand()) {

			int currVal = miniMax(ss);

			// Is it the best we've seen so far?
			if (currVal < val) {
				val = currVal;
			}
		}

		// Return
		return val;
	}

}