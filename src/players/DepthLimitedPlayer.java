package players;

import interfaces.GameState;

import java.util.List;

import data.Line;

/**
 * A player which implements MiniMax with alphabeta pruning and a limit
 * to the depth when making it's moves.
 * 
 * @author Barry
 *
 */
public class DepthLimitedPlayer extends AbstractPlayer {

	/**
	 * Default constructor
	 */
	public DepthLimitedPlayer() {
		super();
	}
	
	@Override
	public Line makeMove(GameState gs) {
		/* Variables */
		int bestScore = -1;
		Line bestLine = null;
		interrupted = false;
	
		/*
		 *  Sleep for half a second.  This simply stops AI vs. AI
		 *  matches being over in a flash.
		 */
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {}
		
		if (interrupted) return null;
		
		// Go through all the successor states
		for (Line ls : gs.getRemainingLines()) {
			// Value of adding this line
			int val;
			
			// Clone of the game
			GameState clone = gs.clone();
			clone.addLine(ls);
			
			// Get the value of this addition
			val = miniMax(clone, Integer.MIN_VALUE, Integer.MAX_VALUE, 3);

			// Check if it is the best 
			if (val > bestScore) {
				bestScore = val;
				bestLine = ls;
			}
		}
		
		return bestLine;
	}
	
	@Override
	public String getDescription() {
		return "This player will play at a level " +
				"suitable for super advanced players.";
	}

	@Override
	public String getName() {
		return "DepthLimited Player";
	}
	
	@Override
	public void reset() {
		interrupt();
	}
	
	/**
	 * @param gs
	 * 		The current game state
	 * @param alpha
	 * 		The current value of alpha.
	 * 		(The best alternative for max)
	 * @param beta
	 * 		The current value of beta.
	 * 		(The best alternative for min)
	 * @param depth
	 * 		The current depth of execution.
	 * @return
	 * 		The value of the state
	 */
	private int miniMax(GameState gs, int alpha, int beta, int depth) {

		// Check for terminal state
		if(gs.expand().isEmpty()) {
			return gs.getValue(); 
		}
		if (depth == 0) {
			return evaluate(gs);
		}
		
		// Figure out who's turn it is
		if(gs.getPlayer() == 1) { 
			return getMaxValue(gs, alpha, beta, depth); 
		}
		return getMinValue(gs, alpha, beta, depth);
	}
	
	/**
	 * @param gs
	 * 		The current game state.
	 * @param alpha
	 * 		The current value of alpha.
	 * 		(The best alternative for max)
	 * @param beta
	 * 		The current value of beta.
	 * 		(The best alternative for min)
	 * @param depth
	 * 		The current depth of execution.
	 * @return
	 * 		The game state with the best heuristic value for this player.
	 */
	private int getMaxValue(GameState gs, int alpha, int beta, int depth) {
		
		// Minimal game value for max
		int val = Integer.MIN_VALUE;
		// Go through all successor states
		for (GameState ss : gs.expand()) {

			int currVal = miniMax(ss, alpha, beta, depth - 1);
			
			if (currVal > val) {
				val = currVal;
			}
			if (currVal >= beta) {
				return val;
			}
			if (currVal > alpha) {
				alpha = currVal;
			}

		}
		
		// Return
		return val;
	}
	
	/**
	 * @param gs
	 * 		The current game state.
	 * @param alpha
	 * 		The current value of alpha.
	 * 		(The best alternative for max)
	 * @param beta
	 * 		The current value of beta.
	 * 		(The best alternative for min)
	 * @param depth
	 * 		The current depth of execution.
	 * @return
	 * 		The game state with the worst heuristic value for this player.
	 * 		(Simulates the other player)
	 */
	private int getMinValue(GameState gs, int alpha, int beta, int depth) {

		// Maximal game value for min
		int val = Integer.MAX_VALUE;
		// Go through all successor states
		for (GameState ss : gs.expand()) {
			
			int currVal = miniMax(ss, alpha, beta, depth - 1);

			if (currVal < val) {
				val = currVal;
			}
			if (currVal <= alpha) {
				return val;
			}
			if (currVal < beta) {
				beta = currVal;
			}
		}
		
		// Return
		return val;
	}
	
	/*
     * This method calculates the value of the game after all
     * available boxes have been taken.  It is not a great
     * evaluation function, but if it is called at a depth
     * of three in the Mini-Max tree, it will be able to
     * spot a double-cross move and thus beat EasyAI pretty
     * much every time.
     */
    private int evaluate(GameState game) {
        int chain = chainLength(game);
        return game.getValue() + (game.getPlayer()==1 ? chain : -chain);
    }
    

    /*
     * This method calculates the number of available boxes
     */
    private int chainLength(GameState game) {
        GameState safe = game.clone();
        List<Line> lines = safe.getRemainingLines();    
        for (Line line : lines) {
            int moveScore = safe.moveScore(line);
            if (moveScore > 0) {
                safe.addLine(line);
                return moveScore + chainLength(safe);
            }
        }
        return 0;
    }

}
